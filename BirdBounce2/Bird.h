//
//  Bird.h
//  AniBird
//
//  Created by Daniel Burke on 1/24/13.
//  Copyright 2013 Daniel Burke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Bird : CCSprite

@property (nonatomic, retain) CCSprite *bird;
@property (nonatomic, assign) NSString *birdColor;
@property (nonatomic, retain) CCAction *flutterAction;
@property (nonatomic, retain) CCAction *moveAction;
@property (nonatomic, retain) CCSpriteBatchNode *spriteSheet;
@property (nonatomic, retain) NSMutableArray *flutterAnimFrames;

- (id)initWithBirdColor:(NSString *)color;
- (void)flutterLeft;
- (void)flutterRight;
- (void)flutter;
- (void)bounce:(float)cloudAngle;
- (void)drop;


@end
