#import "cocos2d.h"
#import "Box2D.h"

#define PTM_RATIO 32.0

@interface HelloWorldLayer : CCLayer {
    b2World *_world;
    b2Body *_groundBody;
    b2Fixture *_bottomFixture;
    b2Fixture *_ballFixture;
    
    b2Body *_paddleBody;
    b2Fixture *_paddleFixture;
    
    b2MouseJoint *_mouseJoint;
    CCSprite *_paddle;
    
    b2PolygonShape paddleShape;
    b2BodyDef paddleBodyDef;
    b2FixtureDef paddleShapeDef;
    
}

+ (id)scene;

@end
